const base = require("@getfixturecom/config/tailwind-preset");
module.exports = {
  ...base,
  content: [...base.content, "../../packages/ui/**/*.{js,ts,jsx,tsx}"],
};
