import type { App } from "@getfixturecom/types/App";

import _package from "./package.json";

export const metadata = {
  name: "Stripe",
  description: _package.description,
  installed: !!(
    process.env.STRIPE_CLIENT_ID &&
    process.env.NEXT_PUBLIC_STRIPE_PUBLIC_KEY &&
    process.env.STRIPE_PRIVATE_KEY
  ),
  slug: "stripe",
  category: "payment",
  logo: "/apps/stripe.svg",
  rating: 4.6,
  trending: true,
  reviews: 69,
  imageSrc: "/apps/stripe.svg",
  label: "Stripe",
  publisher: "getfixture.com",
  title: "Stripe",
  type: "stripe_payment",
  url: "https://getfixture.com/",
  docsUrl: "https://stripe.com/docs",
  variant: "payment",
  verified: true,
  email: "help@getfixture.com",
} as App;

export * as api from "./api";
