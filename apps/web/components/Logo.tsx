export default function Logo({ small, icon }: { small?: boolean; icon?: boolean }) {
  return (
    <h1 className="inline">
      <strong>
        {icon ? (
          <img className="mx-auto w-9" alt="Fixture" title="Fixture" src="/cal-com-icon-white.svg" />
        ) : (
          <img
            className={small ? "h-6 w-auto" : "h-6 w-auto"}
            alt="Fixture"
            title="Fixture"
            src="/logo.svg"
          />
        )}
      </strong>
    </h1>
  );
}
