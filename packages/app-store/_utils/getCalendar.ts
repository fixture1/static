import { Credential } from "@prisma/client";

import logger from "@getfixturecom/lib/logger";
import { Calendar } from "@getfixturecom/types/Calendar";

import appStore from "..";

const log = logger.getChildLogger({ prefix: ["CalendarManager"] });

export const getCalendar = (credential: Credential | null): Calendar | null => {
  if (!credential) return null;
  const { type: calendarType } = credential;
  const calendarApp = appStore[calendarType.split("_").join("") as keyof typeof appStore];
  if (!(calendarApp && "lib" in calendarApp && "CalendarService" in calendarApp.lib)) {
    log.warn(`calendar of type ${calendarType} does not implemented`);
    return null;
  }
  const CalendarService = calendarApp.lib.CalendarService;
  return new CalendarService(credential);
};
