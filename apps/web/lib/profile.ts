import crypto from "crypto";

export const defaultAvatarSrc = function ({ email, md5 }: { md5?: string; email?: string }) {
  if (!email && !md5) return "";

  if (email && !md5) {
    md5 = crypto.createHash("md5").update(email).digest("hex");
  }

  return `https://upload.wikimedia.org/wikipedia/en/8/86/Avatar_Aang.png`;
};
