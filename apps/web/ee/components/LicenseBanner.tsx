import { XIcon } from "@heroicons/react/outline";
import { BadgeCheckIcon } from "@heroicons/react/solid";
import { Trans } from "react-i18next";

import { Dialog, DialogTrigger } from "@getfixturecom/ui/Dialog";

import { useLocale } from "@lib/hooks/useLocale";

import ConfirmationDialogContent from "@components/dialog/ConfirmationDialogContent";

export default function LicenseBanner() {
  const { t } = useLocale();

  if (process.env.NEXT_PUBLIC_LICENSE_CONSENT === "agree") {
    return null;
  }

  return <></>;

  function DialogContent() {
    return <></>;
  }
}
