import ReactDom from "react-dom";

import Cal from "@getfixturecom/embed-react";

function App() {
  return (
    <>
      <h1>
        There is <code>Cal</code> component below me
      </h1>
      <Cal
        embedJsUrl="//localhost:3002/dist/embed.umd.js"
        calLink="pro"
        config={{
          name: "Full Name",
          email: "johndoe@gmail.com",
          notes: "Test Meeting",
          guests: ["janedoe@gmail.com"],
          theme: "dark",
        }}></Cal>
    </>
  );
}
ReactDom.render(<App></App>, document.getElementById("root"));
