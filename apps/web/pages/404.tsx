import { BookOpenIcon, CheckIcon, DocumentTextIcon } from "@heroicons/react/outline";
import { ChevronRightIcon } from "@heroicons/react/solid";
import { GetStaticPropsContext } from "next";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";

import { useLocale } from "@lib/hooks/useLocale";

import { HeadSeo } from "@components/seo/head-seo";

import { ssgInit } from "@server/lib/ssg";

export default function Custom404() {
  const { t } = useLocale();

  const router = useRouter();
  const username = router.asPath.replace("%20", "-");

  const links = [
    {
      title: t("documentation"),
      description: t("documentation_description"),
      icon: DocumentTextIcon,
      href: "https://docs.getfixture.com",
    },
    {
      title: t("blog"),
      description: t("blog_description"),
      icon: BookOpenIcon,
      href: "https://getfixture.com/blog",
    },
  ];

  const [url, setUrl] = useState("https://getfixture.com/signup?username=");
  useEffect(() => {
    setUrl(`https://getfixture.com/signup?username=${username.replace("/", "")}`);
  }, [router.query]);

  const isSubpage = router.asPath.includes("/", 2);
  const isSignup = router.asPath.includes("/signup");
  const isgetfixturecom = process.env.NEXT_PUBLIC_WEBAPP_URL === "https://app.getfixture.com";

  return (
    <>
      <HeadSeo
        title="fixture"
        description={t("404_page_not_found")}
        nextSeoProps={{
          nofollow: true,
          noindex: true,
        }}
      />
    </>
  );
}

export const getStaticProps = async (context: GetStaticPropsContext) => {
  const ssr = await ssgInit(context);

  return {
    props: {
      trpcState: ssr.dehydrate(),
    },
  };
};
