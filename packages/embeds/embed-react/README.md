# cal-react

Makes the embed available as a React component.

To add the embed on a webpage built using React. Follow the steps

```bash
yarn add @getfixturecom/embed-react
```

```jsx
import Cal from "@getfixturecom/embed-react"
<Cal></Cal>
```
